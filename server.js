
const express=require('express');
const bodyParser=require('body-parser');
const adminRoutes=require('./router/admin');
const userRoutes=require('./router/user');


var app=express();

app.set('view engine', 'pug');

app.use(bodyParser.urlencoded({
    extended: true
  }));
app.use(bodyParser.json());





const port=process.env.port||8080;
app.listen(port,()=>console.log(`listening to ${port} ..`));
app.use('/',userRoutes);
app.use('/',adminRoutes);


app.get('/', (req, res) => {
  const s = {};

  if(req.query.s) s.message = {
    message: "Registration succesful. Log in to continue",
    type: "success"
  }

  if(req.query.e) s.message = {
    message: "Login failed",
    type: "error"
  }

  res.render('home', s)
});

app.get('/arts', (req, res) => res.render('arts'));
app.get('/form', (req, res) => {
  const e = {};

  if(req.query.e) e.message = {
    message: "Registration failed",
    type: "error"
  }

  res.render('form', e)
});
app.get('/index.css', (req, res) => res.sendFile(__dirname + '/views/index.css'));
app.get('/img/:file', (req, res) => {
  const file = req.params.file;
  res.sendFile(__dirname + '/img/' + file);
})

